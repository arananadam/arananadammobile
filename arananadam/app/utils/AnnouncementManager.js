

export default class AnnouncementManager {

    static currentAnnouncement = new AnnouncementManager();

    _announcementId=-1;

    /**
     * @returns {AnnouncementManager}
     */
    static getInstance() {
        if (this.currentAnnouncement == null) {
            this.currentAnnouncement = new AnnouncementManager();
        }

        return this.currentAnnouncement;
    }

    getAnnouncementId()
    {
       return this._announcementId;
    }

     setAnnouncementId(announcement)
    {
        this._announcementId = announcement;
    }
}