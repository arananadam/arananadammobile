
export default class CreationManager {

    static creationManager = new CreationManager();

    tempTeam = null;

    /**
     * @returns {CreationManager}
     */
    static getInstance() {
        if (this.creationManager == null) {
            this.creationManager = new CreationManager();
        }

        return this.creationManager;
    }

    getTempTeam()
    {
       return this.tempTeam;
    }

     setTempTeam(tempTeam)
    {
        this.tempTeam = tempTeam;
    }
}