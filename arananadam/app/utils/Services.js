
import UserManager from '../utils/UserManager';

import {Alert } from 'react-native';
import {  USER_REQUEST ,ANNOUNCEMENT_REQUEST,SPORT_TYPE_REQUEST,ANNOUNCEMENT_OFFER_REQUEST} from 'resources/paths';

export function loginRequest(email, password, succes) {

    fetch(USER_REQUEST + 'login/', 
    {
        
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
            
            
        }
        });

}

export function announcementRequest(succes) {

    fetch(ANNOUNCEMENT_REQUEST + 'getReduced/', 
    {
        
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}

export function getAnnouncementWithNameRequest(id,succes) {

    fetch(ANNOUNCEMENT_REQUEST + 'getWithName='+id+'/', 
    {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}

export function getAnnouncement(succes)
{
    fetch(ANNOUNCEMENT_REQUEST, 
    {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });
}

export function getSportTypes(succes)
{
    fetch(SPORT_TYPE_REQUEST, 
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
        })
        .then(response => {
            if (response.ok) {
                response.json().then(json => {
                    succes(json);
                });
              }
            else
            {
            }
            });
}


export function getUsersRequest(succes)
{

    fetch(USER_REQUEST, 
        {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
        })
        .then(response => {
            if (response.ok) {
                response.json().then(json => {
                    succes(json);
                });
              }
            else
            {
            }
            });
}

export function announcementCreateRequest(succes,announcementInsert,game,locationCord,playerUsersWithoout)
{
    var locationType={id:1,type:'Announcement'};
    var playerUsers = playerUsersWithoout.push(UserManager.getInstance().id);
    var tempInsertDTO= Object.assign(announcementInsert,{game});
    var locationCordinates = Object.assign({},locationCord,{locationType});
    //Alert.alert('if:' , JSON.stringify(locationCordinates));
    var temp2InsertDTO=Object.assign(tempInsertDTO,{locationCordinates});
    var AnnouncementInsertDTO=Object.assign(temp2InsertDTO,{playerUsers:[...playerUsers]});

   // Alert.alert('if:' , JSON.stringify(AnnouncementInsertDTO));

    fetch(ANNOUNCEMENT_REQUEST, 
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body:JSON.stringify(AnnouncementInsertDTO),
        })
        .then(response => {
            if (response.ok) {
                response.json().then(json => {
                    succes(json);
                });
              }
            else
            {
            }
            });
}

export function userSignUpRequest(succes,User)
{
    
    fetch(USER_REQUEST, 
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body:JSON.stringify(User),
        })
        .then(response => {
            if (response.ok) {
                response.json().then(json => {
                    succes(json);
                });
              }
            else
            {
            }
            });
}

export function announcementRequestAnnouncerUser(succes,id) {

    fetch(ANNOUNCEMENT_REQUEST + 'getReducedUserId='+id+'/', 
    {
        
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}

export function announcementOfferAnnouncerRequest(succes,id) {

    fetch(ANNOUNCEMENT_OFFER_REQUEST + 'offerannouncerid='+id+'/', 
    {
        
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}

export function ratingRequest(succes,id) {

    fetch(USER_REQUEST + 'findRating='+id+'/', 
    {
        
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}

export function getUserWithId(succes,id) {

    fetch(USER_REQUEST+id+'/', 
    {
        
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}

export function sendOfferRequest(succes,offer)
{
    
    fetch(ANNOUNCEMENT_OFFER_REQUEST, 
        {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body:JSON.stringify(offer),
        })
        .then(response => {
            if (response.ok) {
                response.json().then(json => {
                    succes(json);
                });
              }
            else
            {
            }
            });
}

export function acceptOffer(succes,id,status) {

    fetch(ANNOUNCEMENT_OFFER_REQUEST+'accept='+id+'/status='+status, 
    {
        
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
    })
    .then(response => {
        if (response.ok) {
            response.json().then(json => {
                succes(json);
            });
          }
        else
        {
          
        }
        });

}