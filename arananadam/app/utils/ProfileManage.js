

export default class ProfileManage {

    static profileManage = null;



    /**
     * @returns {ProfileManage}
     */
    static getInstance() {
        if (this.profileManage == null) {
            this.profileManage = new ProfileManage();
        }

        return this.profileManage;
    }
    static setInstance(user)
    {
        this.profileManage = user;
    }
}