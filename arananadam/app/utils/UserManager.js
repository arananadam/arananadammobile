

export default class UserManager {

    static currentUser = null;



    /**
     * @returns {UserManager}
     */
    static getInstance() {
        if (this.currentUser == null) {
            this.currentUser = new UserManager();
        }

        return this.currentUser;
    }
    static setInstance(user)
    {
        this.currentUser = user;
    }
}