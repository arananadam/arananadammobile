export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const PASSWORD_REGEX = /^[a-zA-Z0-9]{6,16}$/;

export const EMPTY_REGEX = /(?!.*((\r\n|\n|\r)$)|(^(\r\n|\n|\r))|^\s*$)/gm;

export const WHITESPACE_REGEX = /^\S*$/;

export const NAME_REGEX = /^.{2,20}$/;

export const CHAR_REGEX = /[a-zA-Z ]+/;
