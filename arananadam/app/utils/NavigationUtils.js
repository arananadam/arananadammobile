import { Navigation } from 'react-native-navigation';
import { BackHandler } from 'react-native';

import Entrance from 'screens/entrance';
import Welcome from 'screens/welcome';
import SignUp from 'screens/signup';
import Announcement from 'screens/announcement'
import AnnouncementDetails from 'screens/announcementdetails'
import AnnouncementCreate from 'screens/announcementCreate'
import UserProfile from 'screens/userProfile'
import MyAnnouncements from 'screens/myAnnouncements'
import OffersForAnnouncements from 'screens/offersForAnnouncements'

export const screens ={
    Entrance: 'arananadam.Entrance',
    Welcome: 'arananadam.Welcome',
    SignUp: 'arananadam.SignUp',
    Announcement: 'arananadam.Announcement',
    AnnouncementDetails: 'arananadam.AnnouncementDetails',
    AnnouncementCreate: 'arananadam.AnnouncementCreate',
    UserProfile: 'arananadam.UserProfile',
    MyAnnouncements : 'arananadam.MyAnnouncements',
    OffersForAnnouncements: 'arananadam.OffersForAnnouncements',
}

export const screensForDrawer =[
    {screen: 'arananadam.Announcement', name: 'Announcements'},
    {screen: 'arananadam.AnnouncementCreate', name: 'Create Announcement'},
    {screen : 'arananadam.UserProfile', name: 'Profile'},
    {screen: 'arananadam.MyAnnouncements', name:'My Announcements'},
    {screen: 'arananadam.OffersForAnnouncements', name: 'Offers For Announcements'}
]

export function registerScreens() {
  Navigation.registerComponent('arananadam.Entrance', () => Entrance);
  Navigation.registerComponent('arananadam.Welcome', () => Welcome);
  Navigation.registerComponent('arananadam.SignUp', () => SignUp);
  Navigation.registerComponent('arananadam.Announcement', () => Announcement);
  Navigation.registerComponent('arananadam.AnnouncementDetails', () => AnnouncementDetails);
  Navigation.registerComponent('arananadam.AnnouncementCreate', () => AnnouncementCreate);
  Navigation.registerComponent('arananadam.UserProfile', () => UserProfile);
  Navigation.registerComponent('arananadam.MyAnnouncements', () => MyAnnouncements);
  Navigation.registerComponent('arananadam.OffersForAnnouncements', () => OffersForAnnouncements);
}

export function startApp() {
    Navigation.startSingleScreenApp({
        screen: {
            screen: screens.Welcome,
            navigatorStyle: {navBarHidden: true}
        }
    });
}

export function navigate(parent,screen,method) {
    parent.props.navigator.resetTo({
        screen: screen,
        navigatorStyle: {navBarHidden: true},
        animated: true, 
        animationType: 'fade',
      });
}

export function setAndroidBackButtonNavigation(parent,screen)
{

    BackHandler.addEventListener('hardwareBackPress', function () {
        if(parent === 'exit') {
            BackHandler.exitApp();
            return;
        }
        parent.props.navigator.resetTo({
            screen: screen,
            navigatorStyle: {navBarHidden: true},
            animated: true, 
            animationType: 'fade',
          });
          return true;
    });

}
