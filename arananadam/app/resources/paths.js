
// android emu ip
//export const HOST_URL = 'http://10.0.2.2:8080/';
//export const HOST_URL = 'http://kaanece.com:8080/arananadam/api/';

export const HOST_URL = 'http://10.0.2.2:8080/api/';

export const USER_REQUEST = HOST_URL + 'user/';

export const TEAM_REQUEST = HOST_URL + 'team/';

export const GAME_REQUEST = HOST_URL + 'game/';

export const ANNOUNCEMENT_REQUEST = HOST_URL + 'announcement/';

export const LOCATION_REQUEST = HOST_URL + 'location/';

export const SPORT_TYPE_REQUEST = HOST_URL + 'sporttype/';

export const ANNOUNCEMENT_OFFER_REQUEST = HOST_URL + 'announcementoffer/';
;
