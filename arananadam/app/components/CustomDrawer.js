import React, {Component } from 'react';
import {
  View, Text,Alert,ListView,StyleSheet, Button
} from 'react-native';
import { Container, Row } from 'native-base';
import { screens, navigate } from 'utils/NavigationUtils';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from 'components/CustomTextInput';
const containerStyles = require('theme/ContainerStyles');
import CustomButton from 'components/CustomButton';
import {loginRequest, announcementRequest} from 'utils/Services';
import {verticalScale} from 'utils/SizeUtils';
import StepIndicator from 'react-native-step-indicator';
import AnnouncementManager from 'utils/AnnouncementManager';
import Accordion from 'react-native-collapsible/Accordion';
const colors = require('resources/colors');
import ActionButton from 'react-native-circular-action-menu';
import Icon from 'react-native-vector-icons/FontAwesome';
import Drawer from 'react-native-drawer';
import { screensForDrawer } from '../utils/NavigationUtils';
import ProfileManage from 'utils/ProfileManage';
import UserManager from 'utils/UserManager';

        

        export default class CustomDrawer extends Component
        {
        
            constructor(props)
            {
                super(props);
                this.screenTempDrawer=screensForDrawer;
                this._drawer=null;
                this.isOpenDrawer=false;
                this.renderDrawerContent=this.renderDrawerContent.bind(this);
                this.renderDrawerNavigationScreenButtons=this.renderDrawerNavigationScreenButtons.bind(this);
                this.toggleDrawer=this.toggleDrawer.bind(this);
                this.navigateCustom=this.navigateCustom.bind(this);
            }


            closeControlPanel = () => {
                this._drawer.close()
              };
              openControlPanel = () => {
                this._drawer.open()
              };
          
          
              toggleDrawer()
              {
                if(this.isOpenDrawer)
                {
                    this.closeControlPanel();
                }
                else
                {
                    this.openControlPanel();
                }
              }

              navigateCustom(iScreen,screen)
              {
                  if(screen===screens.UserProfile)
                  {
                    ProfileManage.setInstance(UserManager.getInstance());
                  }
                  navigate(iScreen,screen);
              }
              renderDrawerNavigationScreenButtons()
              {
                return(
                    this.screenTempDrawer.map(
                  (screenDrawer,index) => {
                    return(
                      <View style={{padding:5}} key={index}>
                    <Button   title={screenDrawer.name} style={{backgroundColor:colors.darkGreen}} onPress={() => {this.navigateCustom(this.props.instantScreen,screenDrawer.screen)}}>
                    </Button>
                    </View>
                    
                  );
                  }
                )
                );
              }
          
              renderDrawerContent()
              {
                return(
                  <Container style={{backgroundColor:colors.lightGreen}}>
                  <View style={{alignItems:'center', alignSelf:'center',justifyContent:'center',borderBottomWidth:2}}>
                    <Text style={{fontSize:40, fontFamily:'vincHand'}}>Aranan Adam
                    </Text>
                  </View>
                  <View style={{marginTop:10}}>
                  {this.renderDrawerNavigationScreenButtons()}
                  </View>
                  </Container>
                );
              }



        render() {
            return (
        <Drawer
          acceptTap={true}
          styles={drawerStyles}
          openDrawerOffset={100}
          tweenHandler={Drawer.tweenPresets.parallax}
        ref={(ref) => this._drawer = ref}
        content={this.renderDrawerContent()}
        >
        {this.props.children}
          </Drawer>
          )

        }

    }

    const drawerStyles = {
        drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
      }