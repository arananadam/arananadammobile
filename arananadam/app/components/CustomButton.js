import React, { Component } from 'react';
import { Button, Text } from 'native-base';

import {verticalScale} from 'utils/SizeUtils';
const buttonStyles = require('theme/ButtonStyles');
const colors = require('resources/colors');

export default class CustomButton extends Component {


    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Button onPress={this.props.onPress} block transparent style={[buttonStyles.ghostButtonStyle, this.props.buttonStyle]}>
                <Text style={{ color: colors.white }}> {this.props.buttonText} </Text>
            </Button>
        );
    }

}
