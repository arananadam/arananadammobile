import React, { Component } from 'react';

import { View, Text, TextInput, Dimensions } from 'react-native';
import {verticalScale} from 'utils/SizeUtils';

const colors = require('resources/colors');
const textStyles = require('theme/TextStyles');
const textInputStyles = require('theme/TextInputStyles');
const containerStyles = require('theme/ContainerStyles');

export default class CustomTextInput extends Component {


    constructor(props) {
        super(props);
        this.state = ({
            hasError: false,
            message: false,
            inputText: '',
            isFocused: false,
            isBlurred: true,
        });
        this.validate = this.validate.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.setTextChange = this.setTextChange.bind(this);
    }

    componentDidMount() {
        this.props.validate(this.validate);
    }

    setTextChange = (text) => {
        this.props.onTextChange(text);
    }

    validate() {
        const validators = this.props.validators;
        let hasError = false;
        let message = '';
        for (let validator of validators) {
            let regex = validator.regex;
            hasError = !regex.test(this.state.inputText);
            if (hasError) {
                message = validator.message;
                break;
            }
        }
        this.setState({ hasError, message });

        return hasError;
    }
    onFocus(isFocused) {
        this.setState({ isFocused });
    }

    onBlur() {
        this.setState({ isFocused: false });
        this.validate();
    }


    render() {
        let { isFocused, hasError } = this.state;
        let color = hasError ? colors.ErrorColor : (isFocused ? colors.white : '#e5e5e5');
        return (
            <View style={this.props.style}>
                <View style={{
                    flexDirection: 'column',
                     marginHorizontal: verticalScale(60)
                }}>
                    <View style={{ flexDirection: 'column' }}>
                        <View style={{ flexDirection: 'row', marginLeft: verticalScale(3) }} >
                            <Text style={[textStyles.inputText, { color: color, marginLeft:verticalScale(5) }]} >
                                {this.props.placeholder} 
                                </Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TextInput autoCapitalize="none"
                                underlineColorAndroid={'transparent'}
                                style={[textInputStyles.TextInput, { color: colors.white, width: verticalScale(Dimensions.get('window').width * (0.7) ),borderBottomColor: color, borderBottomWidth: isFocused ? 2 : 1}]}
                                placeholder={this.props.hintText}
                                onFocus={(isFocused) => this.setState({ isFocused })}
                                onBlur={() => this.onBlur()}
                                onChangeText={(inputText) => { this.setState({ inputText }); this.setTextChange(inputText); }}
                                secureTextEntry={this.props.isPassword} />
                        </View>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', marginHorizontal: verticalScale(60) }}>
                    <Text style={{ color: colors.ErrorColor, width: verticalScale(Dimensions.get('window').width * (0.7)) }} >{this.state.message} </Text>
                </View>
            </View>

        );
    }
}
