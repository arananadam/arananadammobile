import React, { Component } from 'react';
import { Button, Text } from 'native-base';

import { TextInput, View, Alert, StyleSheet, Picker } from 'react-native';
import {verticalScale} from 'utils/SizeUtils';
const buttonStyles = require('theme/ButtonStyles');
const colors = require('resources/colors');

export default class CustomRowsForTeam extends Component {


    constructor(props) {
        super(props);
        this.state={
            selectedPicker:null
        }
        this.changeOptional=this.changeOptional.bind(this);
    }

    changeOptional(val)
    {
        if(val!==0)
        {
            this.setState({selectedPicker: val}); this.props.updateTeamData(val,this.props.keyId);
        }
    }
    render() {

        return (
            <View style={{flexDirection:'row',borderWidth:1,borderColor:colors.white, justifyContent:'center',alignItems:'center',marginBottom:5}}>
            <View style={{height: 50, width:230}}>
            <Picker 
                itemStyle={{color:colors.white}}
                selectedValue={this.state.selectedPicker}
                onValueChange={(itemValue, itemIndex) => {this.changeOptional(itemValue);}}>
                <Picker.Item label='Please select a player.' value={0} />
                 {this.props.users.map( user => <Picker.Item key={user.id} label={user.name} value={user.id}/>)}

            </Picker>
            </View>
            <Button onPress={() => this.props.onPress(this.props.keyId)}>
                <Text>DeleteRow</Text>
            </Button>
            </View>
        );
    }

}