import { StyleSheet } from 'react-native';

var colors = require('resources/colors');

module.exports = StyleSheet.create({

    primaryDarkBlueContainer: {
        backgroundColor: colors.Green,
    },
    ItemsOnCenterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    }

});