import { StyleSheet } from 'react-native';

var colors = require('resources/colors');


module.exports = StyleSheet.create({

    TitleText: {
        fontSize: 50,
        color: '#ffffff',
        fontFamily: 'customfont',
        fontWeight: 'bold',        
    },

    FooterText: {
        fontSize: 12,
        color: colors.white,
    },
    ErrorText: {
        marginLeft: 60,
        color: colors.ErrorColor,
        fontSize: 12,
    },
    InputText: {
        fontSize: 15,
        height: 18,
        backgroundColor: 'transparent',
    },

});