import { StyleSheet } from 'react-native';


module.exports = StyleSheet.create({

  textInput: {
    fontSize: 16,
    height: 40,
    color: "#ffffff",
    width: 200,
    marginBottom: -10
  },
  inputLayout: {
    marginHorizontal: 60,
    backgroundColor: 'transparent',
    textAlignVertical: 'center',
    textAlign: 'left',
    padding: 0
  }
}
);