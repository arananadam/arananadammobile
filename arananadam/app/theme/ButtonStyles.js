import { StyleSheet } from 'react-native';

var colors = require('resources/colors');

module.exports = StyleSheet.create({

    ghostButtonStyle: {
        borderWidth:1,
        borderColor: colors.white,
    }

});