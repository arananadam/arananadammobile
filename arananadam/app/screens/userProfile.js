import React, { Component } from 'react';
import { Container, Row } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput, View, Alert,Text, StyleSheet } from 'react-native';
import ActionButton from 'react-native-circular-action-menu';

import CustomButton from 'components/CustomButton';
import CustomTextInput from 'components/CustomTextInput';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { CHAR_REGEX, NAME_REGEX } from 'utils/Validators';
import { screens, navigate, setAndroidBackButtonNavigation } from 'utils/NavigationUtils';
import {verticalScale} from 'utils/SizeUtils';
import {getAnnouncementWithNameRequest,getAnnouncement,ratingRequest} from 'utils/Services';
import MapView,{Marker}  from 'react-native-maps';
import AnnouncementManager from 'utils/AnnouncementManager';
import ProfileManage from 'utils/ProfileManage';
import CustomDrawer from 'components/CustomDrawer'
import StarRating from 'react-native-star-rating';

import Icon from 'react-native-vector-icons/FontAwesome';
const containerStyles = require('theme/ContainerStyles');
const colors = require('resources/colors');


export default class UserProfile extends Component
{
    constructor(props)
    {
        super(props);
        this._drawer=null;
        this.state={
            user:ProfileManage.getInstance(),
            starCount: 0,
        };
        this.succes=this.succes.bind(this);
        
    }
    componentDidMount()
    {
      this.startRatingRequest();
    }

    startRatingRequest()
    {
      ratingRequest(this.succes,this.state.user.id);
    }

    succes(data)
    {

            //data = JSON.stringify(data);
      if(data !== null && data !== undefined)
      {
       //Alert.alert('if:' , JSON.stringify(data))
       this.setState({
        starCount: data
      });
      }
      else
      {
         //Alert.alert('else:' , data); 
      }

    }
    render()
    {
        return(
            
            <CustomDrawer ref={(ref) => this._drawer=ref} instantScreen={this} >
            <Container style={containerStyles.primaryDarkBlueContainer}>
            
            <View style={{height:50,backgroundColor:colors.darkGreen,flexDirection:'row',alignItems:'center',padding:10}}>

            <Icon.Button name="bars" backgroundColor={colors.lightGreen}  style={{width:30,alignItems:'center'}} onPress={() => {this._drawer.toggleDrawer()}}>
            </Icon.Button>
           
                <Text style={[styles.textHeader,{fontSize:20, fontWeight:'bold',marginLeft:10}]}>
                {this.state.user.name+' '+this.state.user.surname}
                </Text>
                </View>
                <KeyboardAwareScrollView style={{padding:10}}>
                <StarRating
        disabled={false}
        maxStars={5}
        rating={this.state.starCount}
      />
      <View style={{borderWidth:1,padding:3,marginTop:10}}>
        <Text style={{fontSize:20}}>
          E-Mail: {this.state.user.email}
        </Text>
      </View>

      <View style={{borderWidth:1,padding:3,marginTop:10}}>
        <Text style={{fontSize:20}}>
          Phone Number: {this.state.user.phoneNumber}
        </Text>
      </View>

        </KeyboardAwareScrollView>
            </Container>

            
            </CustomDrawer>
        );
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.Green,
    },
    title: {
      textAlign: 'center',
      fontSize: 22,
      fontWeight: '300',
      marginBottom: 20,
    },
    header: {
      backgroundColor: colors.Green,
      padding: 10,
      borderColor: colors.black,
      borderWidth: 1
    },
    headerText: {
      textAlign: 'center',
      fontSize: 16,
      fontWeight: '500',
    },
    content: {
      padding: 5,
      backgroundColor: colors.lightGreen,
      justifyContent: 'flex-end',
      alignItems: 'center',
      height:100,
    },
    active: {
      backgroundColor: 'rgba(255,255,255,1)',
    },
    inactive: {
      backgroundColor: 'rgba(245,252,255,1)',
    },
    selectors: {
      marginBottom: 10,
      flexDirection: 'row',
      justifyContent: 'center',
    },
    selector: {
      backgroundColor: '#F5FCFF',
      padding: 10,
    },
    activeSelector: {
      fontWeight: 'bold',
    },
    selectTitle: {
      fontSize: 14,
      fontWeight: '500',
      padding: 10,
    },
  });