import React, {Component } from 'react';
import {
  View, Text,Alert,ListView,StyleSheet, Button
} from 'react-native';
import { Container, Row } from 'native-base';
import { screens, navigate } from 'utils/NavigationUtils';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from 'components/CustomTextInput';
const containerStyles = require('theme/ContainerStyles');
import CustomButton from 'components/CustomButton';
import {loginRequest, announcementRequest} from 'utils/Services';
import {verticalScale} from 'utils/SizeUtils';
import StepIndicator from 'react-native-step-indicator';
import AnnouncementManager from 'utils/AnnouncementManager';
import Accordion from 'react-native-collapsible/Accordion';
const colors = require('resources/colors');
import ActionButton from 'react-native-circular-action-menu';
import Icon from 'react-native-vector-icons/FontAwesome';
import Drawer from 'react-native-drawer';
import { screensForDrawer } from '../utils/NavigationUtils';
import CustomDrawer from 'components/CustomDrawer'

var values = require('lodash.values');

export default class Announcement extends Component
{
  
    constructor(props) {
        super(props);
        this.screensDrawerTemp=screensForDrawer;
        this._drawer=null;
        this.isOpenDrawer=false;
        this.state = {
          isLoading:true,
          itemList: 
            [{
            name: 'Loading',
            description: "Loading",
            announcementId: -1,
          }]
        };
        this.StartRequest = this.StartRequest.bind(this);
        this.succes=this.succes.bind(this);
        this.onPressButtonFunction=this.onPressButtonFunction.bind(this);
        this.renderActionButton=this.renderActionButton.bind(this);
      }
      componentDidMount()
      {
       this.StartRequest();
      }
      
      onPressButtonFunction()
      {
        navigate(this, screens.AnnouncementDetails);
      }

      

      StartRequest() {
        //const hasEmailError = this.validateEmail();
       // const hasPasswordError = this.validatePassword();
       // if (!hasEmailError && !hasPasswordError) {
            announcementRequest(this.succes);
       // }
    }
    succes(data)
    {
    
                //data = JSON.stringify(data);
        if(data !== null && data !== undefined)
        {
           //Alert.alert('if:' , JSON.stringify(data))
           this.setState({itemList:data,isLoading:false});
          
         }
         else
        {
             Alert.alert('else:' , data); 
             }
    }   



    _renderHeader(section) {
      return (
        <View style={styles.header}>
          <Text style={styles.headerText}>{section.name}</Text>
        </View>
      );
    }


    renderActionButton()
    {
      return(
      <ActionButton
                buttonColor="rgba(231,76,60,1)"
                onPress={() => {navigate(this, screens.AnnouncementCreate)}}
                />)
    }

    _renderContent = (section) => {
      let announ= AnnouncementManager.getInstance();
      return (
        <View style={[styles.content,{ flexDirection:'row'}]} >
          <Text style={{fontSize:20, flex: 1}}>{section.description}</Text>
          <CustomButton style={{ flex: 1}} buttonStyle={{height :35}} buttonText={'Ayrıntı'} onPress={() =>  {announ.setAnnouncementId(section.announcementId);navigate(this, screens.AnnouncementDetails)}}/>
        </View>
      );
    }
      render() {
        return (
          <CustomDrawer ref={(ref) => this._drawer=ref} instantScreen={this} >
          <Container style={styles.container}>
           <View style={{height:50,backgroundColor:colors.darkGreen,flexDirection:'row',alignItems:'center',padding:10}}>

            <Icon.Button name="bars" backgroundColor={colors.lightGreen}  style={{width:40,alignItems:'center'}} onPress={() => {this._drawer.toggleDrawer()}}>
            </Icon.Button>
           
                <Text style={[styles.textHeader,{fontSize:20, fontWeight:'bold',marginLeft:10}]}>
                    Announcements
                </Text>
                </View>
                <KeyboardAwareScrollView style={{padding:10}}>
            <Accordion
        sections={this.state.itemList}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this.onChangeFunction}
      />
      <Text>
        {JSON.stringify(screensForDrawer)}
        </Text>
        </KeyboardAwareScrollView>
                {this.state.isLoading ? null : this.renderActionButton()}
                
          </Container>
          </CustomDrawer>
        );
      }
    

}

const drawerStyles = {
  drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.Green,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: colors.Green,
    padding: 10,
    borderColor: colors.black,
    borderWidth: 1
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  content: {
    padding: 5,
    backgroundColor: colors.lightGreen,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height:100,
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
});