import React, { Component } from 'react';
import { Container, Row } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TouchableOpacity,TextInput, View, Alert,Text, StyleSheet } from 'react-native';
import ActionButton from 'react-native-circular-action-menu';

import CustomButton from 'components/CustomButton';
import CustomTextInput from 'components/CustomTextInput';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { CHAR_REGEX, NAME_REGEX } from 'utils/Validators';
import { screens, navigate, setAndroidBackButtonNavigation } from 'utils/NavigationUtils';
import {verticalScale} from 'utils/SizeUtils';
import {getAnnouncementWithNameRequest,getAnnouncement,getUserWithId,sendOfferRequest} from 'utils/Services';
import MapView,{Marker}  from 'react-native-maps';
import AnnouncementManager from 'utils/AnnouncementManager';
import CustomDrawer from 'components/CustomDrawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-datepicker';
import ProfileManage from 'utils/ProfileManage';
import UserManager from '../utils/UserManager';
const containerStyles = require('theme/ContainerStyles');
const colors = require('resources/colors');


export default class AnnouncementDetails extends Component {

    constructor(props) {
        
        super(props);
        this._drawer=null;
        this.mapRef=null;
        this.state = {
            announcerUser:{name:""},
            announcement:[{announcementName:""}],
            region: {
                latitude: 41.015137,
                longitude: 28.979530,
                latitudeDelta: 0.0722,
                longitudeDelta: 0.0321,
              },
              announcements:null,
        };
        this.StartRequest = this.StartRequest.bind(this);
        this.succes=this.succes.bind(this);
        this.onRegionChangeComplete = this.onRegionChangeComplete.bind(this);
        this.getInitialState = this.getInitialState.bind(this);
        this.succesForLocation=this.succesForLocation.bind(this);
        this.renderMarkers=this.renderMarkers.bind(this);
        this.moveToRegion=this.moveToRegion.bind(this);
        this.getUserSucces=this.getUserSucces.bind(this);
        this.offerSucces=this.offerSucces.bind(this);
        this.offerButton=this.offerButton.bind(this);
    }

    renderMarkers()
    {
        //let announ= AnnouncementManager.getInstance();
       // Alert.alert('if:' , JSON.stringify(this.state.announcements));
        return (this.state.announcements.map(announcements => (
               
                <Marker
               // onPress={() => {announcements.id===this.state.announcement.id ? null : announ.setAnnouncementId(announcements.id); navigate(this, screens.AnnouncementDetails)}}
                key = {announcements.id}
                coordinate={announcements.locationCordinates}
                title={announcements.game.gameName}
                description={announcements.description}
                />
                )));
        
    }

    componentDidMount()
    {
        this.StartRequest();
    }
    getUserSucces(data){
        if(data !== null && data !== undefined)
        {
           //Alert.alert('if:' , JSON.stringify(data))
           this.setState({announcerUser:data});
         }
         else
        {
             Alert.alert('else:' , data); 
             }
    }
    StartRequest() {
        
        getAnnouncement(this.succesForLocation);

    }
    succesForLocation(data)
    {
    
                //data = JSON.stringify(data);
        if(data !== null && data !== undefined)
        {
           //Alert.alert('if:' , JSON.stringify(data))
           this.setState({announcements:data});
         }
         else
        {
             Alert.alert('else:' , data); 
             }
    } 
    succes(data)
    {
    
                //data = JSON.stringify(data);
        if(data !== null && data !== undefined)
        {
           //Alert.alert('if:' , JSON.stringify(data))
           this.setState({announcement:data});
           this.mapRef.animateToRegion(data.locationCordinates,1);
           getUserWithId(this.getUserSucces,this.state.announcement.announcerUser);
        }
        else
        {
             Alert.alert('else:' , data); 
        }
    } 

    moveToRegion()
    {
        let announ =AnnouncementManager.getInstance();
        getAnnouncementWithNameRequest(announ.getAnnouncementId(),this.succes);
    }

    getInitialState() {
        return {
          region: {
            latitude: 41.015137,
            longitude: 28.979530,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
        };
      }
      
      onRegionChangeComplete(region) {
        this.setState({ region:region });
      }

      offerButton()
      {
        sendOfferRequest(this.offerSucces,{announcement:this.state.announcement.id,offerPlayer:UserManager.getInstance().id,description:'AndroidTest'});
      }
      offerSucces(data)
      {
        if(data !== null && data !== undefined)
        {
           Alert.alert('if:' , JSON.stringify(data))
           navigate(this,screens.Announcement);
        }
        else
        {
             Alert.alert('else:' , data); 
        }
      }

    render(){
            return(
                <CustomDrawer ref={(ref) => this._drawer=ref} instantScreen={this} >
                <Container style={{backgroundColor:colors.Green}}>
                <View style={{height:50,backgroundColor:colors.darkGreen,flexDirection:'row',alignItems:'center',padding:10}}>

            <Icon.Button name="bars" backgroundColor={colors.lightGreen}  style={{width:30,alignItems:'center'}} onPress={() => {this._drawer.toggleDrawer()}}>
            </Icon.Button>
           
                <Text style={[styles.textHeader,{fontSize:20, fontWeight:'bold',marginLeft:10}]}>
                {this.state.announcement.name}
                </Text>
                </View>
                <View style={{height:200}}>
                <MapView
                onMapReady={this.moveToRegion}
                ref={map => this.mapRef = map}
                style={{flex:1,...StyleSheet.absoluteFillObject}}
      region={this.state.region}
      onRegionChangeComplete={this.onRegionChangeComplete}
    >
        {this.state.announcements===undefined ?  null : (this.state.announcements===null ? null : this.renderMarkers())}

    </MapView>
    
                </View>
                <KeyboardAwareScrollView style={{padding:10}}>
                <View style={{flex:1, marginTop:10}}>
                <View style={{borderWidth:1}}>
                    <Text style={{fontWeight:'bold', padding:10}}>
                        Maç İsmi: {this.state.announcement.name}
                    </Text>
                </View>
                <TouchableOpacity
                onPress={() => {ProfileManage.setInstance(this.state.announcerUser);navigate(this,screens.UserProfile)}}
                >
                <View style={{borderWidth:1,marginTop:10}}>
                    <Text style={{fontWeight:'bold', padding:10}}>
                        İlanı Oluşturan: {this.state.announcerUser.name + " " + this.state.announcerUser.surname}
                    </Text>
                </View>
                </TouchableOpacity>
                <View style={{flexDirection:'row',flex:1, marginTop:10}}>
                <DatePicker
                    disabled={true}
                    style={{flex:1}}
                    date={this.state.announcement.time}
                    mode="datetime"
                    placeholder="Maç Tarihi"
                    format="YYYY-MM-DD hh:mm:ss"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                    dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                    },
                    dateInput: {
                    marginLeft: 36
                    }
                    }}
                    />
                </View>
                <View style={{borderWidth:1,marginTop:10}}>
                    <Text style={{fontWeight:'bold', padding:10}}>
                        Aranan Oyuncu: {this.state.announcement.numSearchingPlayers}
                    </Text>
                </View>

                <View style={{borderWidth:1,marginTop:10}}>
                    <Text style={{fontWeight:'bold', padding:10}}>
                        Kişi Başı Ücret: {this.state.announcement.costForPlayer}
                    </Text>
                </View>
                
                <View style={{borderWidth:1,marginTop:10}}>
                    <Text style={{fontWeight:'bold', padding:10}}>
                        Açıklama: {this.state.announcement.description}
                    </Text>
                </View>
                <CustomButton buttonStyle={{ alignSelf: 'center', width: verticalScale(250),marginTop:10,marginBottom:20 }} buttonText={'Başvur'} onPress={this.offerButton}>

                </CustomButton>
                    </View>
                </KeyboardAwareScrollView>
                </Container>
                </CustomDrawer>
               
            );
        }

}
const styles = StyleSheet.create({
    textHeader:{
        fontFamily:'Cochin',
        fontSize: 20,
    },
});