import React, { Component } from 'react';
import { Container, Footer, Row, Col } from 'native-base';
import { Platform ,View,Text} from 'react-native';


import CustomButton from 'components/CustomButton';
import { screens, navigate, setAndroidBackButtonNavigation } from 'utils/NavigationUtils';
import {verticalScale} from 'utils/SizeUtils';

const containerStyles = require('theme/ContainerStyles');
const colors = require('resources/colors');


export default class Welcome extends Component {

    constructor(props)
    {
        super(props);
        if (Platform.OS === 'android') {
            setAndroidBackButtonNavigation('exit');
            }
    }

    render() {
        return (
            <Container style={containerStyles.primaryDarkBlueContainer}>
                <Container>
                <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <Text style={{  fontSize: verticalScale(50), color: colors.white }} >
                            Aranan Adam
                        </Text>
                    </View>
                </Container>
                <Container>
                    <Row  >
                        <Col style={{ marginLeft: verticalScale(10), marginRight: verticalScale(10), alignSelf: 'flex-end' }}>
                            <CustomButton onPress={() => navigate(this, screens.Entrance ) } buttonText={'Giriş Yap'} />
                        </Col>
                        <Col style={{ marginLeft: verticalScale(10), marginRight: verticalScale(10), alignSelf: 'flex-end' }}>
                            <CustomButton onPress={() => navigate(this, screens.SignUp ) } buttonText={'Kayıt Ol'} />
                        </Col>
                    </Row>
                </Container>
            </Container>
        );
    }
}

