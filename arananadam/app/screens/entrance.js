import React, {Component } from 'react';
import {
  View, Text,Alert,FlatList
} from 'react-native';
import { Container, Row } from 'native-base';
import { screens, navigate } from 'utils/NavigationUtils';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CustomTextInput from 'components/CustomTextInput';
const containerStyles = require('theme/ContainerStyles');
import CustomButton from 'components/CustomButton';
import {loginRequest} from 'utils/Services';
import {verticalScale} from 'utils/SizeUtils';
import UserManager from 'utils/UserManager'
const colors = require('resources/colors');
import AnnouncementManager from 'utils/AnnouncementManager'
export default class Entrance extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: 'asd@asd.com',
        password: 'asd'
    };
  this.loginButtonOnClick = this.loginButtonOnClick.bind(this);
  this.succes=this.succes.bind(this);
  }

 

  loginButtonOnClick() {
    //const hasEmailError = this.validateEmail();
   // const hasPasswordError = this.validatePassword();
   // if (!hasEmailError && !hasPasswordError) {
        loginRequest(this.state.email, this.state.password, this.succes);
   // }
}
succes(data)
{

            //data = JSON.stringify(data);
    if(data !== null && data !== undefined)
    {
       //Alert.alert('if:' , JSON.stringify(data))
       UserManager.setInstance(data);
       navigate(this, screens.Announcement);
     }
     else
    {
         //Alert.alert('else:' , data); 
         }
}   
  
  render() {
    return (
      <KeyboardAwareScrollView
                enableOnAndroid={true}
                extraScrollHeight={verticalScale(30)}
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={containerStyles.primaryDarkBlueContainer}
                scrollEnabled={false}>
                <Container style={containerStyles.primaryDarkBlueContainer}>
                    <Container style={{ marginTop: verticalScale(10) }}>
                    <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <Text style={{  fontSize: verticalScale(50), color: colors.white }} >
                            Aranan Adam
                        </Text>
                    </View>
                    </Container >
                    <Container />
                    <Row style={containerStyles.ItemsOnCenterContainer}>
                        <CustomTextInput
                            placeholder="E-Mail"
                            validators={[
                                { regex: EMPTY_REGEX, message: 'Empty' },
                                { regex: WHITESPACE_REGEX, message: 'Boşluk' },
                                { regex: EMAIL_REGEX, message: 'Geçersiz' }]}
                            onTextChange={(email) => this.setState({ email })}
                            validate={func => this.validateEmail = func} />
                    </Row>
                    <Row style={containerStyles.ItemsOnCenterContainer}>
                        <CustomTextInput
                            placeholder={'Password'}
                            validators={[
                                { regex: EMPTY_REGEX, message: 'Empty' },
                                { regex: WHITESPACE_REGEX, message: 'Boşluk' },
                                { regex: PASSWORD_REGEX, message: 'Geçersiz' }]}
                            isPassword={true}
                            onTextChange={(password) => this.setState({ password })}
                            validate={func => this.validatePassword = func} />
                    </Row>
                    <Row style={containerStyles.ItemsOnCenterContainer} >
                    <CustomButton onPress={this.loginButtonOnClick} buttonStyle={{ alignSelf: 'center', width: verticalScale(250) }} buttonText={'Login'} />
                    </Row>
                </Container >
            </KeyboardAwareScrollView>
    );
  }
}

