import React, { Component } from 'react';
import { Container, Row } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput, View, Alert } from 'react-native';


import CustomButton from 'components/CustomButton';
import CustomTextInput from 'components/CustomTextInput';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { CHAR_REGEX, NAME_REGEX } from 'utils/Validators';
import { screens, navigate, setAndroidBackButtonNavigation } from 'utils/NavigationUtils';
import {verticalScale} from 'utils/SizeUtils';
import {userSignUpRequest} from 'utils/Services';

const containerStyles = require('theme/ContainerStyles');
const colors = require('resources/colors');


export default class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            email: '',
            password: ''
        };
        this.succes=this.succes.bind(this);
        this.signUpButton=this.signUpButton.bind(this);
    }

    signUpButton()
    {
        
        const hasValidateNameError= this.validateName();
        const hasValidateSurNameError = this.validateSurName();
        const hasEmailError = this.validateEmail();
        const hasPasswordError = this.validatePassword();
        if (!hasEmailError && !hasPasswordError && !hasValidateNameError && !hasValidateSurNameError) {
            this.user={
                name:this.state.name,
                surname:this.state.surname,
                email:this.state.email,
                password:this.state.password
            }
            userSignUpRequest(this.succes,this.user);
        }
    }

    succes(data)
    {
        if(data !== null && data !== undefined && data.id !== null && data.id !== undefined)
        {
            //Alert.alert('if:' , JSON.stringify(data))
            Alert.alert('Kayıt' ,'Kayıt Başarılı !' );
            navigate(this, screens.Entrance);
        }
        else
        {
            Alert.alert('else:' , data); 
        }
    }
  
    render() {
        return (
            <KeyboardAwareScrollView
                enableOnAndroid={true}
                extraScrollHeight={verticalScale(30)}
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={containerStyles.primaryDarkBlueContainer}
                scrollEnabled={false}>
                <Container style={containerStyles.primaryDarkBlueContainer}>
                    <Row style={[containerStyles.ItemsOnCenterContainer,{marginTop:verticalScale(40)}]}>
                        <CustomTextInput
                            placeholder={'İsim'}
                            validators={[
                                { regex: EMPTY_REGEX, message: 'Boş' },
                                { regex: CHAR_REGEX, message: 'Numara İçeremez'},
                                { regex: NAME_REGEX, message: 'Geçersiz' }]}
                            onTextChange={(name) => this.setState({ name })}
                            validate={func => this.validateName = func} />
                    </Row>
                    <Row style={containerStyles.ItemsOnCenterContainer}>
                        <CustomTextInput
                            placeholder={'Soyisim'}
                            validators={[
                                { regex: EMPTY_REGEX, message: 'Boş' },
                                { regex: CHAR_REGEX, message: 'Numara İçeremez' },
                                { regex: NAME_REGEX, message: 'Geçersiz' }]}
                            onTextChange={(surname) => this.setState({ surname })}
                            validate={func => this.validateSurName = func} />
                    </Row>
                    <Row style={containerStyles.ItemsOnCenterContainer}>
                        <CustomTextInput
                            placeholder="Email"
                            validators={[
                                { regex: EMPTY_REGEX, message: 'Boş' },
                                { regex: WHITESPACE_REGEX, message: 'Numara İçeremez' },
                                { regex: EMAIL_REGEX, message: 'Geçersiz' }]}
                            onTextChange={(email) => this.setState({ email })}
                            validate={func => this.validateEmail = func} />
                    </Row>
                    <Row style={containerStyles.ItemsOnCenterContainer}>
                        <CustomTextInput
                            placeholder={'Şifre'}
                            validators={[
                                { regex: EMPTY_REGEX, message: 'Boş' },
                                { regex: WHITESPACE_REGEX, message: 'Boşluk İçeremez' },
                                { regex: PASSWORD_REGEX, message: 'Geçersiz'}]}
                            isPassword={true}
                            onTextChange={(password) => this.setState({ password })}
                            validate={func => this.validatePassword = func} />
                    </Row>
                    <Row style={containerStyles.ItemsOnCenterContainer} >
                        <CustomButton buttonStyle={{ alignSelf: 'center', width: verticalScale(250) }} buttonText={'Kayıt Ol'} onPress={this.signUpButton}/>
                    </Row>
                    
                </Container >
            </KeyboardAwareScrollView>
        );
    }

}
