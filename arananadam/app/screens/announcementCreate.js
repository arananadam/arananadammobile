import React, { Component } from 'react';
import { Container, Row } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput, View, Alert,Text, StyleSheet, Picker,Button } from 'react-native';

import CustomRowsForTeam from 'components/CustomRowsForTeam';
import CustomButton from 'components/CustomButton';
import CustomTextInput from 'components/CustomTextInput';
import { PASSWORD_REGEX, EMAIL_REGEX, EMPTY_REGEX, WHITESPACE_REGEX } from 'utils/Validators';
import { CHAR_REGEX, NAME_REGEX } from 'utils/Validators';
import { screens, navigate, setAndroidBackButtonNavigation } from 'utils/NavigationUtils';
import {verticalScale} from 'utils/SizeUtils';
import {getAnnouncementWithNameRequest,getAnnouncement,getSportTypes,getUsersRequest, announcementCreateRequest} from 'utils/Services';
import MapView,{Marker}  from 'react-native-maps';
import AnnouncementManager from 'utils/AnnouncementManager'
const containerStyles = require('theme/ContainerStyles');
const colors = require('resources/colors');
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomDrawer from 'components/CustomDrawer';
import {screensForDrawer} from '../utils/NavigationUtils';
import UserManager from 'utils/UserManager';
import DatePicker from 'react-native-datepicker';
import Drawer from 'react-native-drawer';
import Moment from 'moment';

export default class AnnouncementCreate extends Component {
    constructor(props)
    {
        super(props);
        Moment.locale('en');
        this.mapRef=null;
        this._drawer=null;
        this.state={
            teamData: [],
            announcementGameObject:{
                gameName: 'New Announcement',
                time:Moment().format("YYYY-MM-DDThh:mm:ss"),
                length:60,
                totalPlayers:12,
                description:'denemeAndroid',
                sportType:null,
            },
            announcementCreateObject:
            {
                announcerUser: UserManager.getInstance().id,
                numSearchingPlayers:1,
                costForPlayer:10,
                description:'Announcement Description',
            },
            isLoading2:true,
            users:null,
            region: {
                latitude: 41.015137,
                longitude: 28.979530,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              },
            isLoading:true,
            pickerItems:[{
                sportName:"Loading",
                id:-1,
            }]
        }
        this.renderPickerItems=this.renderPickerItems.bind(this);
        this.startPickerRequest=this.startPickerRequest.bind(this);
        this.succes=this.succes.bind(this);
        this.onRegionChangeComplete = this.onRegionChangeComplete.bind(this);
        this.getInitialState = this.getInitialState.bind(this);
        this.renderTeamRows=this.renderTeamRows.bind(this);
        this.userSucces = this.userSucces.bind(this);
        this.getUsersRequest = this.getUsersRequest.bind(this);
        this._handleDeleteButton=this._handleDeleteButton.bind(this);
        this._handleAddButton=this._handleAddButton.bind(this);
        this.removePeople=this.removePeople.bind(this);
        this.updateTeamData=this.updateTeamData.bind(this);
        this.createSucces=this.createSucces.bind(this);
        this.announcementCreateRequestStart=this.announcementCreateRequestStart.bind(this);
        this.validate=this.validate.bind(this);
    }

    createSucces(data)
    {
        Alert.alert('if:' , JSON.stringify(data));
    }
    announcementCreateRequestStart()
    {
        announcementCreateRequest(this.createSucces, this.state.announcementCreateObject,this.state.announcementGameObject,this.state.region,this.state.teamData);
    }
    updateTeamData(selected,key)
    {
        let array = [ ...this.state.teamData ];
        array[key] = {...array[key], id: selected};
        this.setState({ teamData:array });
        Alert.alert('if:' , JSON.stringify(this.state.teamData));
    }

    validate()
    {

    }
    _handleAddButton() {
    
    let newly_added_data = { id:-1 };

    this.setState({
        teamData: [...this.state.teamData, newly_added_data]
    });
    }

_handleDeleteButton(key)
{
    this.removePeople(key);
}
removePeople(e) {
    var array = this.state.teamData;
  array.splice(e, 1);
  this.setState({teamData: array });
}

    getUsersRequest()
    {
        getUsersRequest(this.userSucces);
    }

    userSucces(data)
    {
        this.setState({users:data,isLoading2:false});
    }

    renderTeamRows()
    {
        let added_buttons_goes_here = this.state.teamData.map( (data, index) => {
            return (
                <CustomRowsForTeam key={index} keyId ={index} passData={data} users={this.state.users}  onPress={this._handleDeleteButton} updateTeamData={this.updateTeamData}/>
            )
        });
        return added_buttons_goes_here;
    }
    getInitialState() {
        return {
          region: {
            latitude: 41.015137,
            longitude: 28.979530,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
        };
      }
      onRegionChangeComplete(region) {
        this.setState({ region:region });
      }

    componentDidMount()
    {
        this.startPickerRequest();
        this.getUsersRequest();
    }
    renderPickerItems()
    {
        return this.state.pickerItems.map( pickerItem => <Picker.Item key={pickerItem.id} label={pickerItem.sportName} value={pickerItem.id}/>);
    }

    startPickerRequest()
    {
        getSportTypes(this.succes);
    }

    succes(data)
    {
        //Alert.alert('if:' , JSON.stringify(data))
        this.setState({pickerItems:data,isLoading:false});
        this.valueChangeSportTypePicker(data[0].id)
    }

    valueChangeSportTypePicker(itemValue)
    {
        const announcementGameObject = Object.assign({},this.state.announcementGameObject,{sportType:itemValue}); 
        this.setState({announcementGameObject});
    }

    onDatePickerChange(date)
    {
        const announcementGameObject = Object.assign({},this.state.announcementGameObject,{time:date}); 
        this.setState({announcementGameObject});
    }
    lengthValueChange(length)
    {
        const announcementGameObject = Object.assign({},this.state.announcementGameObject,{length:length}); 
        this.setState({announcementGameObject});
    }
    announcementDescriptionChange(text)
    {
        const announcementGameObject = Object.assign({},this.state.announcementGameObject,{description:text}); 
        const announcementCreateObject = Object.assign({},this.state.announcementCreateObject,{description:text}); 
        this.setState({announcementCreateObject});
        this.setState({announcementGameObject});
    }
    render()
    {
        return(
            <CustomDrawer ref={(ref) => this._drawer=ref} instantScreen={this} >
            <Container style={containerStyles.primaryDarkBlueContainer}>
           <View style={{height:50,backgroundColor:colors.darkGreen,flexDirection:'row',alignItems:'center',padding:10}}>
            <Icon.Button name="bars" backgroundColor={colors.lightGreen}  style={{width:30,alignItems:'center'}} onPress={() => {this._drawer.toggleDrawer()}}>
            </Icon.Button>
            
                <TextInput value={this.state.announcementGameObject.gameName} style={{flex:1, height:40}} multiline={false} onChangeText={(text) =>{ const announcementGameObject = Object.assign({},this.state.announcementGameObject,{gameName:text}); this.setState({announcementGameObject})}}>
                </TextInput>
                </View>
            <View style={{height:200}}>
            <MapView
                ref={map => this.mapRef = map}
                style={{flex:1,...StyleSheet.absoluteFillObject}}
                region={this.state.region}
                onRegionChangeComplete={this.onRegionChangeComplete}
                />
            </View>
            <KeyboardAwareScrollView style={{padding:10}}>
            <View style={{ borderWidth:1 , borderColor:colors.white,marginTop:10,height:60,flexDirection:'column'}} >
            <Text style={{marginLeft:5}}>
                Maç Süresi
            </Text>
            <TextInput keyboardType = 'numeric' value={this.state.announcementGameObject.length.toString()} placeHolder={'Maç Süresi'} style={{ height:40}} multiline={false} onChangeText={(text) =>{ const announcementGameObject = Object.assign({},this.state.announcementGameObject,{length:text}); this.setState({announcementGameObject})}}>
                </TextInput>
            </View>

            <View style={{ borderWidth:1 , borderColor:colors.white,marginTop:10,height:60,flexDirection:'column'}} >
            <Text style={{marginLeft:5}}>
                Toplam Oyuncu
            </Text>
            <TextInput keyboardType = 'numeric' value={this.state.announcementGameObject.totalPlayers.toString()} placeHolder={'Toplam Oyuncu'} style={{ height:40}} multiline={false} onChangeText={(text) =>{ const announcementGameObject = Object.assign({},this.state.announcementGameObject,{totalPlayers:text}); this.setState({announcementGameObject})}}>
                </TextInput>
            </View>

            <View style={{ borderWidth:1 , borderColor:colors.white,marginTop:10,height:60,flexDirection:'column'}} >
            <Text style={{marginLeft:5}}>
                Aranan Oyuncu
            </Text>
            <TextInput keyboardType = 'numeric' value={this.state.announcementCreateObject.numSearchingPlayers.toString()} placeHolder={'Toplam Oyuncu'} style={{ height:40}} multiline={false} onChangeText={(text) =>{ const announcementCreateObject = Object.assign({},this.state.announcementCreateObject,{numSearchingPlayers:text}); this.setState({announcementCreateObject})}}>
                </TextInput>
            </View>


            <View style={{ borderWidth:1 , borderColor:colors.white,marginTop:10,height:60,flexDirection:'column'}} >
            <Text style={{marginLeft:5}}>
                Ücret
            </Text>
            <TextInput keyboardType = 'numeric' value={this.state.announcementCreateObject.costForPlayer.toString()} placeHolder={'Toplam Oyuncu'} style={{ height:40}} multiline={false} onChangeText={(text) =>{ const announcementCreateObject = Object.assign({},this.state.announcementCreateObject,{costForPlayer:text}); this.setState({announcementCreateObject})}}>
                </TextInput>
            </View>


            <View style={{ borderWidth:1 , borderColor:colors.white,marginTop:10,flexDirection:'column'}} >
            <Text style={{marginLeft:5}}>
                Açıklama
            </Text>
            <TextInput value={this.state.announcementGameObject.description} placeHolder={'Maç Süresi'} style={{ height:40}} onChangeText={(text) =>{this.announcementDescriptionChange(text)}}>
                </TextInput>
            </View>

            
           
            <View  style={{borderWidth:1, borderColor:colors.white,marginTop:10}}>
            <Picker 
                itemStyle={{color:colors.white}}
                selectedValue={this.state.announcementGameObject.sportType}
                onValueChange={(itemValue, itemIndex) => this.valueChangeSportTypePicker(itemValue)}>
                {this.state.isLoading ? <Picker.Item  label="Loading" value={-1}/> : this.renderPickerItems()  }
            </Picker>
            </View>
            <View style={{flexDirection:'row',flex:1, marginTop:10}}>
            <DatePicker
                    style={{flex:1}}
                    date={this.state.announcementGameObject.time}
                    mode="datetime"
                    placeholder="select time"
                    format="YYYY-MM-DDThh:mm:ss"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                    dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                    },
                    dateInput: {
                    marginLeft: 36
                    }
                    // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => this.onDatePickerChange(date)}
                    />
                    </View>
                    <View style={{marginTop:10, marginBottom:10}}>
            <Button title='Add Player' style={{height: 40 ,width:100}} onPress={this._handleAddButton}/>
            </View>
            {this.state.isLoading2 ? null : this.renderTeamRows()}
            
            <View style={{marginTop:30,marginBottom:50}}>
            <Button title='Create Announcement' style={{height: 40 ,width:100}} onPress={this.announcementCreateRequestStart}/>
            </View>

            </KeyboardAwareScrollView>
            </Container>
            </CustomDrawer>
        );
    }
}


const drawerStyles = {
    drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
  }